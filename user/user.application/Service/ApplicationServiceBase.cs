﻿using operations.domain.DomainExceptions;
using System;
using System.Collections.Generic;
using System.Text;
using user.application.Model;

namespace user.application.Service
{
    public abstract class ApplicationServiceBase
    {
        public Response<T> Execute<T>(Func<T> func) where T : class
        {
            var response = new Response<T>();
            try
            {
                response.Success = true;
                response.Object = func.Invoke();
            }
            catch (DomainException ex)
            {
                response.Success = false;
                response.Object = null;
                response.Message = ex.Message;
            }
            catch (Exception e)
            {
                //Graben en un log
                response.Success = false;
                response.Object = null;
                response.Message = "Internal error server";
            }
            return response;
        }
    }
}
