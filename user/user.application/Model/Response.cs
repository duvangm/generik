﻿using System;
using System.Collections.Generic;
using System.Text;

namespace user.application.Model
{
    public class Response<TResponse> where TResponse : class
    {
        public TResponse Object { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

    }
}
