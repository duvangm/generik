﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using user.application.Dto;
using user.application.implements.Service;
using user.application.Service;
using user.domain.Model;
using user.domain.Repository;
using user.infrastructure.DataAccess;
using user.infrastructure.Repository;

namespace user.api._DependencyInjection
{
    public static class DependencyInjection
    {
        internal static void Start(this IServiceCollection services, IConfiguration configuration)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(configuration.GetConnectionString("DataBaseConnection"));
            optionsBuilder.EnableSensitiveDataLogging();
            services.AddTransient(s => new AppDbContext(optionsBuilder.Options));
            services.AddTransient(s => new UserRepository(s.GetRequiredService<AppDbContext>()) as IRepositoryBase<User, int>);
            services.AddTransient(s => new UserServices(s.GetRequiredService<IRepositoryBase<User, int>>()) as IServiceBase<UserDto, int>);
        }
    }
}
