﻿using Microsoft.AspNetCore.Mvc;
using user.application.Dto;
using user.application.Model;
using user.application.Service;
using System.Collections.Generic;
using user.api.Controllers;

namespace users.api.Controllers
{
    [Route("api/user")]
    public class UserController : ControllerApp<UserDto, int>
    {
        private readonly IServiceBase<UserDto, int> _userApplicationService;


        public UserController(IServiceBase<UserDto, int> userApplicationService)
        {
            _userApplicationService = userApplicationService;
        }

        [HttpDelete]
        public override Response<UserDto> Delete(int id)
        {
            return _userApplicationService.Delete(id);
        }

        [HttpGet]
        public override Response<IEnumerable<UserDto>> GetAll()
        {
            return _userApplicationService.GetAll();
        }

        [HttpGet("{id}")]
        public override Response<UserDto> GetById(int id)
        {
            return _userApplicationService.GetById(id);
        }

        [HttpPost]
        public override Response<UserDto> Post(UserDto dto)
        {
            return _userApplicationService.Insert(dto);
        }

        [HttpPut]
        public override Response<UserDto> Put(UserDto dto)
        {
            return _userApplicationService.Update(dto);
        }
    }
}
