﻿using System;
using System.Collections.Generic;
using System.Text;

namespace user.infrastructure.Dao
{
    public class UserDao
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public UserDao()
        {

        }
        public UserDao(int id, string name, string password)
        {
            Id = id;
            Name = name;
            Password = password;
        }
    }
}
