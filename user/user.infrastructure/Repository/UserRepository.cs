﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using user.domain.Model;
using user.domain.Repository;
using user.infrastructure.Dao;
using user.infrastructure.DataAccess;

namespace user.infrastructure.Repository
{
    public class UserRepository : RepositoryBase<UserDao, int>, IRepositoryBase<User, int>
    {

        public UserRepository(AppDbContext context) : base(context)
        {
        }

        public User Delete(int key)
        {
            var dao = base.Delete(key);
            return Map(dao);
        }

        public IEnumerable<User> GetAll()
        {
            var dao = base.GetAll();
            return dao.Select(op => Map(op)).ToList();
        }

        public User GetById(int key)
        {
            var dao = base.GetById(key);
            return Map(dao);
        }

        public User Insert(User entity)
        {
            var dto = Map(entity);
            return Map(base.Insert(dto));
        }

        public User Update(User entity)
        {
            var dto = Map(entity);
            return Map(base.Update(dto));
        }

        private UserDao Map(User user)
        {
            return new UserDao
            {
                Id = user.Id,
                Name = user.Name,
                Password = user.Password
            };
        }

        private User Map(UserDao user)
        {
            return new User
            {
                Id = user.Id,
                Name = user.Name,
                Password = user.Password
            };
        }
    }
}