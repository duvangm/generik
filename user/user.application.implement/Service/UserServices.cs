﻿using user.application.Dto;
using user.application.Model;
using user.application.Service;
using user.domain.Model;
using user.domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace user.application.implements.Service
{
    public class UserServices : ApplicationServiceBase, IServiceBase<UserDto, int>
    {
        private readonly IRepositoryBase<User, int> _repositoryBase;

        public UserServices(IRepositoryBase<User, int> repositoryBase)
        {
            _repositoryBase = repositoryBase;
        }

        public Response<UserDto> Delete(int key)
        {
            return Execute(() => {
                //USE CASE
                var entity = _repositoryBase.Delete(key);
                return Map(entity);
            });
        }

        public Response<IEnumerable<UserDto>> GetAll()
        {
            return Execute(() => {
                var entities = _repositoryBase.GetAll();
                return Map(entities);
            });
        }

        public Response<UserDto> GetById(int key)
        {
            return Execute(() => {
                var entity = _repositoryBase.GetById(key);
                return Map(entity);
            });
        }

        public Response<UserDto> Insert(UserDto dto)
        {
            return Execute(() => {
                var entity = Map(dto);
                _repositoryBase.Insert(entity);
                return dto;
            });
        }

        public Response<UserDto> Update(UserDto dto)
        {
            return Execute(() => {
                var entity = Map(dto);
                _repositoryBase.Update(entity);
                return dto;
            });
        }

        private UserDto Map(User user)
        {
            return new UserDto
            {
                Id = user.Id,
               Name = user.Name,
               Password = user.Password
            };
        }

        private User Map(UserDto user)
        {
            return new User
            {
                Id = user.Id,
                Name = user.Name,
                Password = user.Password
            };
        }

        private IEnumerable<UserDto> Map(IEnumerable<User> user)
        {
            var list = new List<UserDto>();
            user?.ToList().ForEach(it => {
                list.Add(Map(it));
            });
            return list;
        }
    }
}
