﻿using System;
using System.Collections.Generic;
using System.Text;
using user.domain.Model;

namespace user.domain.Repository
{
    public interface IUserRepository : IRepositoryBase<User, int>
    {
    }
}
