﻿using System;
using System.Collections.Generic;
using System.Text;

namespace user.domain.Model
{
    public class User
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public bool Gender { get; set; }

        public User()
        {

        }
        public User(int id, string name, string password)
        {
            Id = id;
            Name = name;
            Password = password;
        }

        //Va la lógica de negocio o reglas de negocio.
        //Por ejemplo una regla de negocio es que el Password debe ser mayor a 7 dígitos
        public bool ValidatePasswordLength(string Password)
        {
            return (Password.Length > 7);
        }
    }
}
